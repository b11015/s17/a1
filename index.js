// console.log("hello")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//first function here:
	alert("Welcome to my page")

	let fullName = prompt("Enter Your Full name")
	console.log("Hello " + fullName)

	function ageAndLocation(){
		let aGe = prompt("How old are you")
		let locaTion = prompt("Where do you live");

		console.log("You are " + aGe + "years old");
		console.log("You live in " + locaTion);
	};

	ageAndLocation();

/*

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	let bands = ("Favorite Bands:  ")
	console.log(bands)

	function favoriteBands(){
		let favBand1 = "1. Parokya ni Edgar"
		let favBand2 = "2. IV of Spades"
		let favBand3 = "3. Ben & Ben"
		let favBand4 = "4. Eraserheads"
		let favBand5 = "5. Rivermaya"

		console.log(favBand1);
		console.log(favBand2);
		console.log(favBand3);
		console.log(favBand4);
		console.log(favBand5);
	};

	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	let movies = ("Favorite Movies: ")
	console.log(movies)

	function favoriteMovies1(){
		console.log("1. Spiderman");
		console.log("Rotten Tomtaoes Rating: 97%");
	};

	favoriteMovies1();

	function favoriteMovies2(){
		console.log("2. The Dark Knight");
		console.log("Rotten Tomtaoes Rating: 95%");
	};

	favoriteMovies2();

	function favoriteMovies3(){
		console.log("1. Terminator");
		console.log("Rotten Tomtaoes Rating: 99%");
	};

	favoriteMovies3();

	function favoriteMovies4(){
		console.log("5. I am Legend");
		console.log("Rotten Tomtaoes Rating: 91%");
	};

	favoriteMovies4();

	function favoriteMovies5(){
		console.log("5. Avengers");
		console.log("Rotten Tomtaoes Rating: 92%");
	};

	favoriteMovies5();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:"); 

	console.log("You are friends with: ")
	console.log(friend1);
	console.log(friend2);
	console.log(friend3);

};

printUsers();